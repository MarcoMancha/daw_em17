
SELECT * FROM Clientes_Banca

/*
  Muestra los registros actuales de la tabla Clientes_Banca
 */

BEGIN TRANSACTION Prueba2
INSERT INTO Clientes_Banca VALUES('004','Ricardo Rios Maldonado',19000);
INSERT INTO Clientes_Banca VALUES('005','Pablo Ortiz Arana',15000);
INSERT INTO Clientes_Banca VALUES('006','Luis Manuel Alvarado',18000);


SELECT * FROM Clientes_Banca

/*
  Aparece la tabla actualizada, en cambio en la primera sesión la consulta tarda en cargar.
 */

SELECT * FROM Clientes_Banca where NoCuenta='001'

/*
  Aparece el registro con NoCuenta 001 y un saldo de 9000.
  En la primera sesión a parece desactualizado el registro.
*/

ROLLBACK TRANSACTION Prueba2

SELECT * FROM Clientes_Banca

/*
  Solo aparecen los primeros 3 registros que se insertaron en la primera sesión, esto pasa porque el ROLLBACK cancela los cambios
  hechos por la transa
 */



