CREATE TABLE Clientes_Banca
(
	NoCuenta varchar(5) NOT NULL PRIMARY KEY,
	Nombre varchar(30),
	Saldo numeric(10,2)
)

CREATE TABLE Tipos_Movimientos
(
	ClaveM varchar(2) NOT NULL PRIMARY KEY,
	Descripcion varchar(30)
)

CREATE TABLE Movimientos
(
	NoCuenta varchar(5) NOT NULL FOREIGN KEY REFERENCES Clientes_Banca(NoCuenta),
	ClaveM varchar(2) NOT NULL FOREIGN KEY REFERENCES Tipos_Movimientos(ClaveM),
	Fecha datetime,
	Monto numeric(10,2)
)

BEGIN TRANSACTION Prueba1
INSERT INTO Clientes_Banca VALUES('001', 'Manuel Rios Maldonado', 9000);
INSERT INTO Clientes_Banca VALUES('002', 'Pablo Perez Ortiz', 5000);
INSERT INTO Clientes_Banca VALUES('003', 'Luis Flores Alvarado', 8000);
COMMIT TRANSACTION Prueba1


SELECT * FROM Clientes_Banca

SELECT * FROM CLIENTES_BANCA where NoCuenta='001'

BEGIN TRANSACTION Prueba3
INSERT INTO Tipos_Movimientos VALUES('A','Retiro Cajero Automatico');
INSERT INTO Tipos_Movimientos VALUES('B','Deposito Ventanilla');
COMMIT TRANSACTION Prueba3


BEGIN TRANSACTION Prueba4
INSERT INTO Movimientos VALUES('001','A',GETDATE(),500);
UPDATE Clientes_Banca SET SALDO = SALDO -500
WHERE NoCuenta='001'
COMMIT TRANSACTION Prueba4

SELECT * FROM Tipos_Movimientos

SELECT * FROM Movimientos

SELECT * FROM Clientes_Banca

/*
	Actualizó la tabla de TIPOS DE MOVIMIENTOS agregandole 2 registros e insertó uno nuevo a movimientos y al mismo tiempo hizo
	un update a la tabla clientes_banca al registro con número de cuenta 001.
 */

BEGIN TRANSACTION Prueba5
INSERT INTO Clientes_Banca VALUES('025','Rosa Ruiz Maldonado',9000);
INSERT INTO CLIENTES_BANCA VALUES('036','Luis Camino Ortiz',5000);
INSERT INTO CLIENTES_BANCA VALUES('001','Oscar Perez Alvarado',8000);


IF @@ERROR = 0 AND @@ROWCOUNT = 3
COMMIT TRANSACTION PRUEBA5
ELSE
BEGIN
PRINT 'A transaction needs to be rolled back'
ROLLBACK TRANSACTION PRUEBA5
END

SELECT * FROM Clientes_Banca

BEGIN TRY
BEGIN TRANSACTION Prueba7
INSERT INTO Clientes_Banca VALUES('017','Rosa Ruiz Maldonado',9000);
INSERT INTO CLIENTES_BANCA VALUES('026','Luis Camino Ortiz',5000);
INSERT INTO CLIENTES_BANCA VALUES('001','Oscar Perez Alvarado',8000);
END TRY

BEGIN CATCH
	IF @@TRANCOUNT = 0
	COMMIT TRANSACTION Prueba7
	ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
	ROLLBACK TRANSACTION Prueba7
	END
END CATCH

SELECT * FROM Clientes_Banca

/*
Es una manera de excepción, agarra el error ejecuta algo en caso de que se presente un error.
La transacción hace 2 inserciones a la tabla clientes banca, hace una tercera pero viola la integridad referencial por el primary key.
Primero se ejecutan las 2 inserciones y después en el tercer insert se presenta un error y se hace un rollback de la transaction.
No hubo una modificación en la tabla, no se agregaron los registros 005 y 006, el tercero no se agregó porque tenía la misma llave primaria que otro registro.
 */

SET IMPLICIT_TRANSACTIONS OFF

CREATE PROCEDURE REGISTRAR_RETIRO_CAJERO
	@noCuenta VARCHAR(5),
	@Monto 		NUMERIC(10,2)
AS
	BEGIN TRANSACTION RETIRO_CAJERO
	INSERT INTO Movimientos VALUES(@noCuenta,'A',GETDATE(),@Monto);
	UPDATE Clientes_Banca SET Saldo = (SELECT Saldo FROM Clientes_Banca WHERE NoCuenta = @noCuenta) - @Monto WHERE NoCuenta = @noCuenta
	IF @@ERROR = 0
	COMMIT TRANSACTION REGISTRAR_RETIRO_CAJERO
	ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
	ROLLBACK TRANSACTION REGISTRAR_RETIRO_CAJERO
	END
GO

SELECT * FROM Clientes_Banca
SELECT * FROM Movimientos

EXECUTE REGISTRAR_RETIRO_CAJERO '001',500.00

CREATE PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA
	@noCuenta VARCHAR(5),
	@Monto 		NUMERIC(10,2)
AS
	BEGIN TRANSACTION DEPOSITO_VENTANILLA
	INSERT INTO Movimientos VALUES(@noCuenta,'B',GETDATE(),@Monto);
	UPDATE Clientes_Banca SET Saldo = (SELECT Saldo FROM Clientes_Banca WHERE NoCuenta = @noCuenta) + @Monto WHERE NoCuenta = @noCuenta
	IF @@ERROR = 0
	COMMIT TRANSACTION REGISTRAR_DEPOSITO_VENTANILLA
	ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
	ROLLBACK TRANSACTION REGISTRAR_DEPOSITO_VENTANILLA
	END
GO

SELECT * FROM Clientes_Banca
SELECT * FROM Movimientos

EXECUTE REGISTRAR_DEPOSITO_VENTANILLA '001',1000.00



